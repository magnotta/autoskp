#!/bin/bash

# flags:
# --check-in
# --check-out
# --check-out-with-random-log
# --random-log

if [ -z $1 ]; then
	echo "Provide one of the following: --check-in, --check-out, --random-log or --check-out-with-random-log."
	exit 0
fi

knownArg=false
for param in --check-in --check-out --check-out-with-random-log --random-log; do
	if [ "$1" = "$param" ]; then
		knownArg=true;
		break
	fi
done

if [ "$knownArg" = false ]; then
	echo "Invalid argument: $1"
	exit -1;
fi

script_dir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_dir"

# Read credentials
username=$(sed -n 1p .credentials.txt)
password=$(sed -n 2p .credentials.txt)

# Get initial PHPSESSID
curl https://itd-skp.sde.dk/ -sc cookies.txt

# Login (new PHPSESSID & APISSID)
curl https://itd-skp.sde.dk/admin/login.php -s -b cookies.txt -c cookies.txt --data-urlencode "username=$username" --data-urlencode "password=$password" --data 'action=login' -H 'Referer: https://itd-skp.sde.dk/admin/login.php'

## Logbog
if [ "$1" = "--random-log" ] || [ "$1" = "--check-out-with-random-log" ]; then
	logMsg=$(shuf -n 1 log_messages.txt)
	curl https://itd-skp.sde.dk/student/index.php -sH "Referer: https://itd-skp.sde.dk/student/index.php?page=logbook" --data "page=logbook&action=logbook" --data-urlencode "logbook=$logMsg" -b cookies.txt
	if [ "$1" = "--random-log" ]; then
		rm cookies.txt
		exit 0;
	fi
fi

## Location
# Get JS
js=$(curl 'https://itd-skp.sde.dk/student/index.php?page=student-location' --compressed -H 'Referer: https://itd-skp.sde.dk/admin/login.php' -b cookies.txt)

# Get checksum
checksum=$(echo $js | grep -oP "(?<=checksum: ').*?(?=')")

# Get last_location_id
last_location_id=$(echo $js | grep -oP '(?<=var last_location_id = ).*?(?=;)')

# Check in or out
do_action='1' # const
id=$(echo $js | grep -oP "(?<=id: )\d+") # possibly student id

if [ "$1" = "--check-in" ]; then
	student_location_id=3 # 3: Zone 8
elif [ "$1" = "--check-out" ] || [ "$1" = "--check-out-with-random-log" ]; then
	student_location_id=8 # 8: fyraften
fi

student_name=$(echo $js | grep -oP "(?<=student_name: ').*?(?=')") # must be url-encoded in curl call

majority_payload="do_action=$do_action&id=$id&location_id=$last_location_id&student_location_id=$student_location_id&checksum=$checksum"

response=$(curl 'https://itd-skp.sde.dk/student/handlers/locationHandler.php' -H 'Referer: https://itd-skp.sde.dk/student/index.php?page=student-location' -H 'Origin: https://itd-skp.sde.dk' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -b cookies.txt --data "$majority_payload" --data-urlencode "student_name=$student_name")

rm cookies.txt
