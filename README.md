## Config
Copy and edit sample credentials:
`cp credentials_sample.txt .credentials.txt`  
`vim .credentials`

Optionally modify log entry messages in `log_messages.txt`. Each line is an entry.

## Usage
`./autoSKP.sh --check-in`  
`./autoSKP.sh --check-out`  
`./autoSKP.sh --random-log`  
`./autoSKP.sh --check-out-with-random-log`  

## Suggested crontab:
50 7 * * 1-5 /path/to/autoSKP.sh --check-in  
30 15 * * 1-4 /path/to/autoSKP.sh --check-out-with-random-log  
30 14 * * 5 /path/to/autoSKP.sh --check-out-with-random-log  
